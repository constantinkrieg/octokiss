// script.js
$(document).ready(function () {
    const $gameContainer = $('#game-container');
    const $score = $('#score');
    const $lives = $('#lives');
    const $objective = $('#objective');
    const $timeLeft = $('#time-left');
    let score = 0;
    let lives = 1;
    let objectiveScore = 20; // Set your objective score here
    let timeLimit = 60; // Set your time limit in seconds here
    let timeRemaining = timeLimit;
    let moleAppearanceInterval = 2000; // Initial mole appearance interval
    let bombAppearanceInterval = 2500; // Initial bomb appearance interval
    let gameInterval;
    let timerInterval;

    function getRandomPosition() {
        const containerWidth = $gameContainer.width() - 100;
        const containerHeight = $gameContainer.height() - 100;
        const left = Math.random() * containerWidth;
        const top = Math.random() * containerHeight;
        return { left, top };
    }

    function showMole() {
        const $mole = $('<div class="mole"></div>');
        const position = getRandomPosition();
        $mole.css({ left: position.left, top: position.top });
        $mole.on('click', function () {
            $(this).remove();
            score += 1;
            $score.text(score);
        });
        $gameContainer.append($mole);
        $mole.fadeIn(300, function () {
            setTimeout(function () {
                $mole.fadeOut(300, function () {
                    $(this).remove();
                });
            }, moleAppearanceInterval);
        });
    }

    function showBomb() {
        const $bomb = $('<div class="bomb"></div>');
        const position = getRandomPosition();
        $bomb.css({ left: position.left, top: position.top });
        $bomb.on('click', function () {
            $(this).remove();
            lives -= 1;
            $lives.text(lives);
            if (lives === 0) {
                gameOver(false);
            }
        });
        $gameContainer.append($bomb);
        $bomb.fadeIn(300, function () {
            setTimeout(function () {
                $bomb.fadeOut(300, function () {
                    $(this).remove();
                });
            }, bombAppearanceInterval);
        });
    }

    function startGame() {
        gameInterval = setInterval(showMole, moleAppearanceInterval);
        setInterval(showBomb, bombAppearanceInterval);
        timerInterval = setInterval(updateTimer, 1000);
    }

    function updateTimer() {
        timeRemaining -= 1;
        $timeLeft.text(timeRemaining);
        if (timeRemaining === 0) {
            gameOver(score >= objectiveScore);
        }
    }

    function gameOver(hasReachedObjective) {
        clearInterval(gameInterval);
        clearInterval(timerInterval);
        if (hasReachedObjective) {
            localStorage.setItem('game6', 'true');
            window.history.back();
        } else {
            alert('Raté :(');
        }
        location.reload(); // You can implement a more sophisticated game over screen.
    }

    // Initialize game
    $objective.text(objectiveScore);
    $timeLeft.text(timeRemaining);
    startGame();
});
