const wordToGuess = "AVOCAT";
const maxAttempts = 6;
let currentAttempts = 0;
let guessedWord = Array(wordToGuess.length).fill('_');

$(document).ready(function () {
    updateWordDisplay();

    $(document).on("keyup", function (event) {
        const letter = event.key.toUpperCase();
        if (isLetter(letter) && guessedWord.indexOf(letter) === -1) {
            checkLetter(letter);
        }
    });
});

function isLetter(char) {
    return /^[A-Z]$/i.test(char);
}

function updateWordDisplay() {
    const wordDisplay = guessedWord.join(' ');
    $("#word").text(wordDisplay);
}

function checkLetter(letter) {
    if (wordToGuess.indexOf(letter) !== -1) {
        for (let i = 0; i < wordToGuess.length; i++) {
            if (wordToGuess[i] === letter) {
                guessedWord[i] = letter;
            }
        }
        updateWordDisplay();
        if (guessedWord.join('') === wordToGuess) {
            endGame(true);
        }
    } else {
        currentAttempts++;
        displayIncorrectLetter(letter);
        if (currentAttempts >= maxAttempts) {
            endGame(false);
        }
    }
}

function displayIncorrectLetter(letter) {
    const lettersDiv = $("#letters");
    lettersDiv.append(`<span>${letter}</span>`);
}

function endGame(isWin) {
    $(document).off("keyup");
    if (isWin) {
        localStorage.setItem('game3', 'true');
        window.history.back();
    } else {
        location.reload();
    }
}
