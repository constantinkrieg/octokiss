$(document).ready(function () {
  let flippedCards = [];
  let pairsMatched = 0;
  const totalPairs = 8;
  let cardFlipped = false;
  let timeLeft = 40; // 60 seconds = 1 minute

  // Shuffle the cards
  const cards = $('.card');
  for (let i = cards.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      cards[i].before(cards[j]);
  }

  // Function to update the timer display
  function updateTimer() {
      $('#timer').text(timeLeft);

      if (timeLeft === 0) {
          endGame(false); // Game over, player ran out of time
      } else {
          timeLeft--;
          setTimeout(updateTimer, 1000); // Update the timer every second
      }
  }

  updateTimer(); // Start the timer

  // Function to end the game
  function endGame(isWinner) {
      cardFlipped = true;
      $('.card').off('click'); // Disable card clicking
      if (isWinner) {
        localStorage.setItem('game4', 'true');
        window.history.back();
      } else {
        location.reload();
      }
  }

  $('.card').click(function () {
      if (cardFlipped) return;

      const currentCard = $(this);

      if (!currentCard.hasClass('flipped') && flippedCards.length < 2) {
          currentCard.addClass('flipped');
          flippedCards.push(currentCard);

          if (flippedCards.length === 2) {
              const card1 = flippedCards[0].data('card');
              const card2 = flippedCards[1].data('card');

              if (card1 === card2) {
                  flippedCards[0].addClass('matched');
                  flippedCards[1].addClass('matched');
                  pairsMatched++;

                  if (pairsMatched === totalPairs) {
                      endGame(true); // Player has won
                  }

                  flippedCards = [];
              } else {
                  cardFlipped = true;
                  setTimeout(function () {
                      flippedCards[0].removeClass('flipped');
                      flippedCards[1].removeClass('flipped');
                      cardFlipped = false;
                      flippedCards = [];
                  }, 1000);
              }
          }
      }
  });
});
