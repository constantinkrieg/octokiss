$(document).ready(function () {
    
    // Variables
    let sequence = [];
    let playerSequence = [];
    let round = 0;
    let strictMode = true;
    let gameOn = false;
    let sound;

    // Audio Elements
    const greenSound = document.getElementById("green-sound");
    const redSound = document.getElementById("red-sound");
    const yellowSound = document.getElementById("yellow-sound");
    const blueSound = document.getElementById("blue-sound");
    const errorSound = document.getElementById("error-sound");

    function playRandomSound() {
        const sounds = [greenSound, redSound, yellowSound, blueSound];
        sound = sounds[Math.floor(Math.random() * sounds.length)];
        sound.play();
    }

    
    // Function to play a sound
    function playSound(color) {
        switch (color) {
            case "green":
                sound = greenSound;
                break;
            case "red":
                sound = redSound;
                break;
            case "yellow":
                sound = yellowSound;
                break;
            case "blue":
                sound = blueSound;
                break;
            case "error":
                sound = errorSound;
                break;
            default:
                break;
        }
        sound.play();
    }

    // Function to generate a random color and add it to the sequence
    function addToSequence() {
        const colors = ["green", "red", "yellow", "blue"];
        const randomColor = colors[Math.floor(Math.random() * 4)];
        sequence.push(randomColor);
        $("#round-counter").text(++round);
        playSequence();
    }

    // Function to play the current sequence
    function playSequence() {
        gameOn = false; // Disable player input during sequence playback
        let i = 0;
        const interval = setInterval(function () {
            const color = sequence[i];
            playRandomSound();
            highlightButton(color); // Highlight the button
            setTimeout(() => {
                unhighlightButton(color); // Unhighlight the button
            }, 500);
            i++;
            if (i >= sequence.length) {
                clearInterval(interval);
                setTimeout(enablePlayerInput, 500); // Enable player input after sequence playback
            }
        }, 1000);
    }

        // Function to highlight a button
        function highlightButton(color) {
            $(`.quadrant.${color}`).addClass("active");
        }
    
        // Function to unhighlight a button
        function unhighlightButton(color) {
            $(`.quadrant.${color}`).removeClass("active");
        }
    
        // Function to enable player input
        function enablePlayerInput() {
            playerSequence = [];
            gameOn = true; // Enable player input
        }


           // Function to check player input
           function checkPlayerInput() {
            const index = playerSequence.length - 1;
            if (playerSequence[index] !== sequence[index]) {
                playSound("error");
                if (strictMode) {
                    resetGame();
                } else {
                    setTimeout(function () {
                        playSequence();
                    }, 1000);
                }
            } else if (playerSequence.length === sequence.length) {
                if (round === 10) {
                    localStorage.setItem('game5', 'true');
                    window.history.back();
                    resetGame();
                } else {
                    setTimeout(function () {
                        addToSequence();
                    }, 1000);
                }
            }
        }

    // Function to reset the game
    function resetGame() {
        sequence = [];
        playerSequence = [];
        round = 0;
        strictMode = false; // Reset strict mode
        gameOn = false;
        $("#strict-mode").removeClass("active"); // Unset strict mode indicator
        $("#round-counter").text("0");
        $(".quadrant").off("click");
        location.reload();
    }

    // Start button click event
    $("#start-button").on("click", function () {
        if (!gameOn) {
            gameOn = true;
            addToSequence();
        }
    });

    // Click event for colored buttons
    $(".quadrant").on("click", function () {
        if (gameOn) {
            const color = $(this).attr("class").split(" ")[1];
            playerSequence.push(color);
            playSound(color);
            highlightButton(color);
            setTimeout(() => {
                unhighlightButton(color);
            }, 500);
            checkPlayerInput();
        }
    });

    // Strict mode toggle
    $("#strict-mode").on("click", function () {
        strictMode = !strictMode;
        $(this).toggleClass("active");
    });
});
